package com.twuc.webApp.domain.oneToMany.withJoinColumn;

import javax.persistence.*;

// TODO
//
// ChildEntity 应当具备一个自动生成的 id 以及一个字符串 name。请实现 ChildEntity。ChildEntity 的
// 数据表的参考定义如下：
//
// +───────────────────+──────────────+──────────────────────────────+
// | Column            | Type         | Additional                   |
// +───────────────────+──────────────+──────────────────────────────+
// | id                | bigint       | primary key, auto_increment  |
// | name              | varchar(20)  | not null                     |
// | parent_entity_id  | bigint       | null                         |
// +───────────────────+──────────────+──────────────────────────────+
//
// <--start-
@Entity
public class ChildEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ChildEntity(String name) {
        this.name = name;
    }

    public ChildEntity() {
    }

    @Column(length = 20,nullable = false)
    private String name;
}
// --end->
